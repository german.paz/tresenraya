package tresenraya;

public class tresenraya {
	
	private final char J1 = 'X';
	private final char J2 = 'O';
	private final char VACIO = '-';
	
	//turno actual 
	//true  = J1, false = J2
	private boolean turno;
	//tablero donde vamos a jugar
	private char tablero [][];
	
	
	
	public tresenraya()
	{
		turno = true;
		tablero = new char[3][3];
		inicializarTablero();
	}

	/**
	 * Inicializa el tablero con el simbolo VACIO
	 */
	
	
	private void inicializarTablero()
	{
		for(int i=0; i<tablero.length;i++)
		{
			for(int j=0; j<tablero.length;j++)
			{
				tablero[i][j] = VACIO;
			}
		}
	}
	
	public void ColocarFicha(int fila, int columna) {
        if (turno) {
            this.tablero[fila][columna] = J1;
        } else {
            this.tablero[fila][columna] = J2;
        }
    }
	
	  public boolean tableroLleno() {
	        for (int i = 0; i < tablero.length; i++) {
	            for (int j = 0; j < tablero[0].length; j++) {
	                if (tablero[i][j] == VACIO) {
	                    return false;
	                }
	            }
	        }
	        return true;
	    }
	 
	  public void mostrarTablero() {
		  
	        for (int i = 0; i < this.tablero.length; i++) {
	            for (int j = 0; j < this.tablero[0].length; j++) {
	                System.out.print(this.tablero[i][j] + " ");
	            }
	            System.out.println("");
	        }
	 
	  }
	  
	  
	  public void mostrarTurnoActual() {
	   
	      if (turno) {
	          System.out.println("Le toca al jugador 1");
	      } else {
	          System.out.println("Le toca al jugador 2");
	      }
	   
	  }

	   
	  public void cambiaTurno() {
	      this.turno = !this.turno;
	  }
	  
	  public boolean validarPosicion(int fila, int columna) {
		  
		    if (fila >= 0 && fila < tablero.length && columna >= 0 && columna < tablero.length) {
		        return true;
		    }
		    return false;
		 
		}
		 
		
		public boolean hayValorPosicion(int fila, int columna) {
		    if (this.tablero[fila][columna] != VACIO) {
		        return true;
		    }
		 
		    return false;
		}
	  
	
}
